Inspired by the [pyo3 guide](git@gitlab.com:pohlarized/python-rust-extension-example.git).


# Build
I highly recommend using a venv!

First, install maturin:
```
pip install maturin
```

Now do
```
maturin develop
```

to install the current verision of the library.

