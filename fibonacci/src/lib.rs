use std::i128;

use pyo3::prelude::*;

/// Formats the sum of two numbers as string.
#[pyfunction]
fn sum_as_string(a: usize, b: usize) -> PyResult<String> {
    Ok((a + b).to_string())
}

/// A Python module implemented in Rust.
#[pymodule]
fn fibonacci(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(sum_as_string, m)?)?;
    m.add_function(wrap_pyfunction!(r_fib_rec, m)?)?;
    m.add_function(wrap_pyfunction!(r_fib_iter, m)?)?;
    Ok(())
}

#[pyfunction]
fn r_fib_rec(n: i128) -> PyResult<i128> {
    if n < 2 {
        return Ok(n);
    }
    Ok(r_fib_rec(n - 1).unwrap() + r_fib_rec(n - 2).unwrap())
}

#[pyfunction]
fn r_fib_iter(n: i128) -> PyResult<i128> {
    let mut pre_last = 0;
    let mut last = 1;
    let mut current = 0;
    if n < 2 {
        return Ok(n);
    }
    for _ in 2..=n {
        current = last + pre_last;
        pre_last = last;
        last = current;
    }
    Ok(current)
}
