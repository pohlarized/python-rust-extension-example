import time
from typing import Callable

import fibonacci

def fib_iter(n: int) -> int:
    if n < 2:
        return n
    pre_last = 0
    last = 1
    current = 0
    for _ in range(2, n + 1):
        current = last + pre_last
        pre_last = last
        last = current
    return current

def fib_rec(n: int) -> int:
    if n < 2:
        return n
    return fib_rec(n-1) + fib_rec(n-2)


def time_func(func: Callable[[int], int], arg: int):
    start = time.time()
    res = func(arg)
    stop = time.time()
    print(f'Function {func.__name__}({arg}) took {stop - start} seconds. Result: {res}.')


time_func(fib_iter, 180)
time_func(fibonacci.r_fib_iter, 180)
time_func(fib_rec, 37)
time_func(fibonacci.r_fib_rec, 100)
